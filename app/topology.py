from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.topology import event
from ryu.topology.api import get_all_switch, get_all_link

class Topology(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(Topology, self).__init__(*args, **kwargs)

    @set_ev_cls(event.EventSwitchEnter)
    def get_topology_data(self, ev):
        switch_list = get_all_switch(self)
        switches = [ s.dp.id for s in switch_list]
        links_list = get_all_link(self)
        links = [(
                link.src.dpid, link.dst.dpid,
                { 'port' : link.src.port_no}
        ) for link in links_list]
