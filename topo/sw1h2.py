from mininet.topo import Topo

class Sw1h2(Topo):
    def __init__(self):
        super(self.__class__, self).__init__()

        s1 = self.addSwitch("s1")

        h1 = self.addHost("h1")
        h2 = self.addHost("h2")

        self.addLink(s1, h1, bw=100, loss=2)
        self.addLink(s1, h2, bw=100, loss=2)

topos = { 'Sw1h2' : ( lambda: Sw1h2() ) }
