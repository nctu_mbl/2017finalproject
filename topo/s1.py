from mininet.topo import Topo

class S1(Topo):
    def __init__(self):
        super(self.__class__, self).__init__()

        s1 = self.addSwitch("s1")
        s2 = self.addSwitch("s2")

        h1 = self.addHost("h1")
        h2 = self.addHost("h2")

        self.addLink(s1, s2)
        self.addLink(s1, s2)

        self.addLink(s1, h1)
        self.addLink(s2, h2)

topos = { 'S1' : ( lambda: S1() ) }
