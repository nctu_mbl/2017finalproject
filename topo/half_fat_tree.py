from mininet.topo import Topo

class HalfFatTree(Topo):
    BW_CA = 100
    BW_AE = 100
    BW_EH = 100

    LOSS_CA = 0
    LOSS_AE = 2
    LOSS_EH = 0

    def __init__(self):
        super(self.__class__, self).__init__()

        coreSWs = list(self.createCoreSW())
        agrgSWs = list(self.createAgrgSW())
        edgeSWs = list(self.createEdgeSW())
        hosts = list(self.createHosts())

        ###
        # C : Core Switch
        # A : Aggregation Swtich
        # E : Edge Switch
        # H : Hosts
        self.connCA(coreSWs, agrgSWs)
        self.connAE(agrgSWs, edgeSWs)
        self.connEH(edgeSWs, hosts)

    ###
    # Create Switch: s1001 ~ s1002
    #
    def createCoreSW(self):
        for i in range(1001, 1001 + 2):
            yield self.addSwitch("s{}".format(i))

    ###
    # Create Switch: s2001 ~ s2004
    #
    def createAgrgSW(self):
        for i in range(2001, 2001 + 4):
            yield self.addSwitch("s{}".format(i))

    ###
    # Create Switch: s3001 ~ s3004
    #
    def createEdgeSW(self):
        for i in range(3001, 3001 + 4):
            yield self.addSwitch("s{}".format(i))
    ###
    # Create Host: h4001 ~ h4008
    #
    def createHosts(self):
        for i in range(4001, 4001 + 8):
            yield self.addHost("h{}".format(i))

    ###
    # Connecting between Core Switch and Aggregation Switch
    #
    def connCA(self, coreSWs, agrgSWs):
        for i, a_sw in enumerate(agrgSWs):
            if i % 2 == 0:
                self.addLink(
                    coreSWs[0], a_sw,
                    bw=self.BW_CA, loss=self.LOSS_CA
                )
            else:
                self.addLink(
                    coreSWs[1], a_sw,
                    bw=self.BW_CA, loss=self.LOSS_CA
                )

    ###
    # Connecting between Aggregation Switch  and Edge Switch
    #
    def connAE(self, agrgSWs, edgeSWs):
        for i, e_sw in enumerate(edgeSWs):
            if i % 2 == 0:
                group = i
            else:
                group = i - 1

            toConnAgrg_sws = agrgSWs[group : group+2]

            for a_sw in toConnAgrg_sws:
                self.addLink(
                    a_sw, e_sw,
                    bw=self.BW_AE, loss=self.LOSS_AE
                )

    ###
    # Connecting between Edge Switch and Hosts
    #
    def connEH(self, edgeSWs, hosts):
        for i, host in enumerate(hosts):
            self.addLink(
                edgeSWs[i/2], host,
                bw=self.BW_EH, loss=self.LOSS_EH
                )

topos = { 'halfFatTree' : ( lambda: HalfFatTree() ) }
