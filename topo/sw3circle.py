from mininet.topo import Topo

class Sw3Circle(Topo):
    def __init__(self):
        super(self.__class__, self).__init__()

        s1 = self.addSwitch("s1")
        s2 = self.addSwitch("s2")
        s3 = self.addSwitch("s3")

        h1 = self.addHost("h1")
        h2 = self.addHost("h2")
        h3 = self.addHost("h3")

        self.addLink(s1, s2)
        self.addLink(s2, s3)
        self.addLink(s3, s1)

        self.addLink(s1, h1)
        self.addLink(s2, h2)
        self.addLink(s3, h3)

topos = { 'Sw3Circle' : ( lambda: Sw3Circle() ) }
