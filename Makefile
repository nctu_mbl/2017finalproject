
.PHONY: start mn

start:
	ryu run --observe-links \
		app/index.py

mn-demo:
	sudo mn \
		--custom topo/demo.py \
		--topo mytopo \
		--link tc \
		--controller=remote,ip=127.0.0.1

mn:
	sudo mn \
		--custom topo/fat_tree.py \
		--topo fatTree \
		--link tc \
		--controller=remote,ip=127.0.0.1

mn-half-fat:
	sudo mn \
		--custom topo/half_fat_tree.py \
		--topo halfFatTree \
		--link tc \
		--controller=remote,ip=127.0.0.1

mn-test-s1h2:
	sudo mn \
		--custom topo/sw1h2.py \
		--topo Sw1h2 \
		--link tc \
		--controller=remote,ip=127.0.0.1

mn-test-s1:
	sudo mn \
		--custom topo/s1.py \
		--topo S1 \
		--link tc \
		--controller=remote,ip=127.0.0.1

mn-test-loop:
	sudo mn \
		--custom topo/loop.py \
		--topo mytopo \
		--link tc \
		--controller=remote,ip=127.0.0.1
